/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      colors: {
        indigo: "#4830B0",
        amber: "#EC9109",
        vermilion: "#E78448",
        "light-gray-2": "#A1A1A1",
        "light-gray-1": "#D6D6D6",
        violet: "#6444F0",
      },
    },
  },
  plugins: [],
};
