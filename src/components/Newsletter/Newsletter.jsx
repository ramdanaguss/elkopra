import Image from "next/image";
import Button from "../UI/Button";

import newsletterImage from "@/assets/image/happy-woman.png";

function Newsletter() {
  return (
    <section className="px-[120px] pb-24 pt-64 max-[1300px]:px-[90px] max-[1200px]:px-[50px] max-[600px]:text-center max-[400px]:px-[20px]">
      <div className="relative flex flex-col items-start gap-[17px] rounded-2xl bg-violet px-[53px] py-[70px] max-[600px]:p-[30px] max-[450px]:p-[10px]">
        <h3 className="text-[40px] font-bold text-white max-[450px]:text-4xl">
          Subscribe news letter
        </h3>

        <p className="max-w-[776px] text-lg leading-[40px] text-white max-[1348px]:max-w-[500px] max-[600px]:leading-[23px] max-[450px]:text-base">
          Using a combination of technology, process and talents we focus on our
          users needs and help to accelerate their business at any scale. We
          deliver consistent
        </p>

        <div className="space-y-[14px]">
          <div className="relative max-w-[552px] max-[600px]:flex max-[600px]:flex-col max-[600px]:items-center max-[600px]:gap-5">
            <input
              type="text"
              placeholder="Input email"
              className="h-11 w-full rounded-full px-[26px] placeholder:text-light-gray-2 focus:outline-none"
            />
            <Button
              variant="primary"
              className="absolute right-0 top-0 max-[600px]:static"
            >
              Send Email
            </Button>
          </div>

          <p className="text-xs text-white">
            This site is protected by reCAPTCHA and the Google Privacy Policy
            and Terms of Service apply.
          </p>
        </div>

        <Image
          src={newsletterImage}
          alt="Happy woman"
          height={519}
          className="absolute -right-8 bottom-0 w-auto max-[1032px]:hidden"
        />
      </div>
    </section>
  );
}

export default Newsletter;
