import { useState, useEffect } from "react";
import { createPortal } from "react-dom";

import { createWrapperAndAppendToBody } from "@/utils/createWrapperAndAppendToBody";

function ReactPortal({ children, wrapperId }) {
  const [wrapperElement, setWrapperElement] = useState(null);

  useEffect(() => {
    let element = document.getElementById(`#${wrapperId}`);

    if (!element) {
      element = createWrapperAndAppendToBody("portal");
    }

    setWrapperElement(element);

    return () => {
      if (element) {
        element.parentNode?.removeChild(element);
      }
    };
  }, [wrapperId]);

  if (!wrapperElement) return null;

  return createPortal(children, wrapperElement);
}

export default ReactPortal;
