import Image from "next/image";

import elKopra from "@/assets/icons/el-kopra.svg";
import chevronDown from "@/assets/icons/chevron-down.svg";
import ellipse from "@/assets/icons/ellipse.svg";
import envelope from "@/assets/icons/envelope.svg";
import facebook from "@/assets/icons/facebook.svg";
import instagram from "@/assets/icons/instagram.svg";
import linkedin from "@/assets/icons/linkedin.svg";
import message from "@/assets/icons/message.svg";
import people from "@/assets/icons/people.svg";
import phone from "@/assets/icons/phone.svg";
import pin from "@/assets/icons/pin.svg";
import task from "@/assets/icons/task.svg";
import youtube from "@/assets/icons/youtube.svg";
import indonesia from "@/assets/icons/indonesia.svg";
import heroImage from "@/assets/icons/hero-image.svg";

const svgList = {
  elKopra,
  chevronDown,
  ellipse,
  envelope,
  facebook,
  instagram,
  linkedin,
  message,
  people,
  phone,
  pin,
  task,
  youtube,
  indonesia,
  heroImage,
};

function Svg({ name, alt, ...props }) {
  return <Image src={svgList[name]} alt={alt} {...props} priority />;
}

export default Svg;
