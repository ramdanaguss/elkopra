import Link from "next/link";

function TextLink({ children, href, className }) {
  return (
    <Link
      href={href}
      className={`text-light-gray-1 text-lg font-bold duration-300 hover:text-white active:text-white ${className}`}
    >
      {children}
    </Link>
  );
}

export default TextLink;
