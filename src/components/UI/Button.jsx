import Link from "next/link";

function Button({ type, children, href, onClick, variant, className }) {
  const primaryClasses = variant === "primary" ? "bg-amber" : "";

  const secondaryClasses =
    variant === "secondary"
      ? "border border-white hover:bg-amber hover:border-transparent"
      : "";

  const buttonClasses = `block p-[10px] text-center text-white rounded-full duration-300 w-[166px] ${primaryClasses} ${secondaryClasses} ${className}`;

  if (type === "link") {
    return (
      <Link href={href} className={buttonClasses}>
        {children}
      </Link>
    );
  }

  return (
    <button onClick={onClick} className={buttonClasses}>
      {children}
    </button>
  );
}

export default Button;
