import StatisticsItem from "./StatisticsItem";

function Statistics() {
  return (
    <section className="px-[120px] pt-[25px] max-[1300px]:px-[90px] max-[1200px]:px-[50px] max-[400px]:px-[20px]">
      <div className="flex flex-col gap-[88px] max-[600px]:gap-[45px]">
        <div className="flex flex-col items-center gap-8 text-center">
          <p className="text-2xl font-medium text-white">
            Lorem ipsum dolot amet lorem ipsum
          </p>

          <div className="mb-[34px] flex flex-col items-center gap-5">
            <h2 className="text-5xl font-bold text-white">
              Our <span className="text-amber">Passion</span> What We Do.
            </h2>
            <div className="h-1 w-[119px] bg-vermilion " />
          </div>
        </div>

        <div className="max flex items-center justify-between text-center max-[900px]:flex-col max-[900px]:gap-20">
          <StatisticsItem
            icon="message"
            number="98%"
            alt="message icon"
            desc="Possitive Feedback"
          />
          <StatisticsItem
            icon="people"
            number="120"
            alt="people icon"
            desc="Enthusiastic Fulltime Employee"
          />
          <StatisticsItem
            icon="task"
            alt="task icon"
            number="20"
            desc="Projects Completed"
          />
        </div>
      </div>
    </section>
  );
}

export default Statistics;
