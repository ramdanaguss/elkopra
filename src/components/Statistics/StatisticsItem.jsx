import Svg from "../UI/Svg";

function StatisticsItem({ icon, number, desc, alt }) {
  return (
    <div className="flex w-fit flex-col items-center">
      <span className="mb-8 max-[900px]:mb-4">
        <Svg name={icon} alt={alt} width={51.28} className="h-auto w-auto" />
      </span>
      <p className="mb-6 text-[60px] font-bold text-white max-[900px]:mb-2">
        {number}
      </p>
      <p className="text-2xl text-white">{desc}</p>
    </div>
  );
}

export default StatisticsItem;
