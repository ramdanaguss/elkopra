import CardList from "../Card/CardList";

async function News() {
  const res = await fetch(
    "https://newsapi.org/v2/everything?q=tesla&from=2023-10-15&sortBy=publishedAt&apiKey=ae9d63e642a2462f9304889476937b12",
  );

  const { articles } = await res.json();
  const news = articles.slice(0, 3);

  return (
    <section className="clip-path-box-1 max-[600px]:clip-path-box-0 h-fit w-full bg-white px-[120px] py-[190px] max-[1300px]:px-[90px] max-[1200px]:px-[50px] max-[962px]:py-[300px] max-[600px]:py-[40px] max-[400px]:px-[20px]">
      <div className="mb-[34px] flex flex-col items-center gap-5">
        <h2 className="text-5xl font-bold text-black">Latest News</h2>
        <div className="h-1 w-[119px] bg-vermilion " />
      </div>

      <p className="mb-[91px] text-center text-lg leading-[32px] text-light-gray-2">
        At elKopra we are committed to providing top-notcto cater to all your
        needs. Our team of dedicated professionals is passionate about
        delivering exceptional solutions that exceed expectations.
      </p>

      <CardList list={news}></CardList>
    </section>
  );
}

export default News;
