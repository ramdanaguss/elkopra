import Image from "next/image";

import Button from "@/components/UI/Button";
import heroImage from "@/assets/image/business-woman.png";

function Hero() {
  return (
    <section className="relative flex items-center justify-between rounded-b-full px-[120px] pb-[30px] max-[1300px]:px-[90px] max-[1200px]:px-[50px] max-[1056px]:flex-col max-[400px]:px-[20px]">
      <div className="flex max-w-[600px] flex-col items-start gap-10 pb-[30px] pt-[85px] max-[1056px]:text-center">
        <h1 className="flex flex-col text-7xl font-bold leading-[100px] text-white max-[1150px]:text-6xl max-[1150px]:leading-[80px] max-[600px]:text-5xl">
          <span className="pl-3">
            Generate <span className="text-amber">New</span>
          </span>
          <span className="whitespace-nowrap max-[1430px]:whitespace-normal">
            Digital Cooperation
          </span>
        </h1>

        <p className="leading-40px max-w-[638px] text-2xl text-white max-[1150px]:max-w-[550px] max-[1150px]:text-xl">
          We design and build solutions by connecting ideas and technologhy to
          solve problems and get new ideas to business lifecycle.
        </p>

        <Button variant="primary" className="max-[1056px]:mx-auto">
          Get Started
        </Button>
      </div>

      <Image
        src={heroImage}
        alt="Business woman"
        className="-mr-12 h-auto w-[500px] items-start max-[1150px]:w-[450px] max-[1056px]:-mt-20 max-[1056px]:self-end max-[917px]:hidden"
      />
    </section>
  );
}

export default Hero;
