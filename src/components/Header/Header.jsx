import MobileNavbar from "../Navbar/MobileNavbar";
import Navbar from "../Navbar/Navbar";

function Header() {
  return (
    <header>
      <MobileNavbar />
      <Navbar />
    </header>
  );
}

export default Header;
