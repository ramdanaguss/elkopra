import Link from "next/link";
import Svg from "../UI/Svg";
import TextLink from "../UI/TextLink";

function Footer() {
  return (
    <footer className="flex justify-between bg-violet px-[120px] pb-[120px] pt-[60px] max-[1300px]:px-[90px] max-[1200px]:px-[50px] max-[1000px]:flex-col-reverse max-[1000px]:gap-14 max-[400px]:px-[20px]">
      <div>
        <Svg
          name="elKopra"
          alt="elKopra logo"
          width={130}
          height={37}
          className="mb-[35px] h-auto w-auto"
        />

        <ul className="space-y-4">
          <li className="flex max-w-[332px] items-start gap-[10px]">
            <Svg
              name="pin"
              alt="pin icon"
              width={24}
              height={24}
              className="h-auto w-auto pt-2"
            />
            <p className="leading-[32px] text-white">
              Jl. Terusan Jakarta No.76, Antapani Tengah, Bandung, Jawa Barat,
              40291
            </p>
          </li>

          <li className="flex max-w-[332px] items-start gap-[10px]">
            <Svg
              name="phone"
              alt="phone icon"
              height={24}
              width={24}
              className="h-auto w-auto"
            />
            <p className="text-white">+62 356 7829 873</p>
          </li>

          <li className="flex max-w-[332px] items-start gap-[10px]">
            <Svg
              name="envelope"
              alt="envelope icon"
              height={24}
              width={24}
              className="h-auto w-auto"
            />
            <p className="text-white">elkopra@gmail.com</p>
          </li>
        </ul>
      </div>

      <div className="flex flex-wrap gap-[100px] max-[630px]:gap-[50px] max-[520px]:gap-9">
        <div>
          <h4 className="mb-8 pt-2 text-[22px] font-bold text-white">
            Company
          </h4>

          <ul className="space-y-4">
            <li className="flex max-w-[332px] items-start gap-[10px]">
              <TextLink href="/" className="font-normal text-white">
                About Us
              </TextLink>
            </li>

            <li className="flex max-w-[332px] items-start gap-[10px]">
              <TextLink href="/" className="font-normal text-white">
                Career
              </TextLink>
            </li>

            <li className="flex max-w-[332px] items-start gap-[10px]">
              <TextLink href="/" className="font-normal text-white">
                Contact Us
              </TextLink>
            </li>
          </ul>
        </div>

        <div>
          <h4 className="mb-8 pt-2 text-[22px] font-bold text-white">
            Product
          </h4>

          <ul className="space-y-4">
            <li className="flex max-w-[332px] items-start gap-[10px]">
              <TextLink href="/" className="font-normal text-white">
                elKopra
              </TextLink>
            </li>

            <li className="flex max-w-[332px] items-start gap-[10px]">
              <TextLink href="/" className="font-normal text-white">
                elRaga
              </TextLink>
            </li>

            <li className="flex max-w-[332px] items-start gap-[10px]">
              <TextLink href="/" className="font-normal text-white">
                elResto
              </TextLink>
            </li>
          </ul>
        </div>

        <div>
          <h4 className="mb-8 pt-2 text-[22px] font-bold text-white">
            Company
          </h4>

          <ul className="flex items-center gap-5">
            <li className="flex items-start gap-[10px]">
              <Link href="/">
                <Svg
                  name="linkedin"
                  alt="linkedin icon"
                  width={24}
                  height={24}
                  className="h-auto w-auto pt-2"
                />
              </Link>
            </li>

            <li className="flex items-start gap-[10px]">
              <Link href="/">
                <Svg
                  name="instagram"
                  alt="instagram icon"
                  width={24}
                  height={24}
                  className="h-auto w-auto pt-2"
                />
              </Link>
            </li>

            <li className="flex items-start gap-[10px]">
              <Link href="/">
                <Svg
                  name="facebook"
                  alt="facebook icon"
                  width={24}
                  height={24}
                  className="h-auto w-auto pt-2"
                />
              </Link>
            </li>

            <li className="flex items-start gap-[10px]">
              <Link href="/">
                <Svg
                  name="youtube"
                  alt="youtube icon"
                  width={24}
                  height={24}
                  className="h-auto w-auto pt-2"
                />
              </Link>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
