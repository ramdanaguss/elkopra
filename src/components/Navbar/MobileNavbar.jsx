"use client";

import { useDispatch, useSelector } from "react-redux";

import Button from "@/components/UI/Button";
import ReactPortal from "@/components/UI/ReactPortal";
import Svg from "@/components/UI/Svg";
import TextLink from "@/components/UI/TextLink";

import { getIsOpen, toggleNavbar } from "@/store/navbarSlice";

function MobileNavbar() {
  const isOpen = useSelector(getIsOpen);
  const dispatch = useDispatch();

  if (!isOpen) return null;

  return (
    <ReactPortal>
      <div className="absolute top-0 z-50 h-screen w-screen bg-white">
        <aside className="absolute bottom-0 top-0 h-screen w-[330px]  bg-violet px-4 py-6 shadow-md">
          <header className="flex items-center justify-between">
            <Svg
              name="elKopra"
              alt="elKopra logo"
              width={130}
              height={37}
              className="h-auto w-auto"
            />

            <button
              onClick={() => dispatch(toggleNavbar())}
              className="relative pr-4"
            >
              <div className="absolute h-[3px] w-5 rotate-45 bg-white" />
              <div className="absolute h-[3px] w-5 -rotate-45 bg-white" />
            </button>
          </header>

          <nav className="border-b border-white py-8">
            <ul className="flex flex-col items-start gap-5">
              <li>
                <TextLink href="/">About Us</TextLink>
              </li>
              <li>
                <TextLink href="/" className="flex gap-2">
                  <span>Product</span>
                  <Svg
                    name="chevronDown"
                    alt="chevron icon"
                    width={24}
                    height={24}
                    className="h-auto w-auto"
                  />
                </TextLink>
              </li>
              <li>
                <TextLink href="/">Project</TextLink>
              </li>
              <li>
                <TextLink href="/">Career</TextLink>
              </li>
            </ul>
          </nav>

          <footer className="flex justify-between py-8">
            <Button type="link" href="/" variant="secondary">
              Contact Us
            </Button>

            <div className="flex cursor-pointer items-center gap-2">
              <Svg
                name="indonesia"
                alt="indonesia flag"
                width={24}
                height={24}
                className="h-auto w-auto"
              />
              <Svg
                name="chevronDown"
                alt="chevron icon"
                width={24}
                height={24}
                className="h-auto w-auto"
              />
            </div>
          </footer>
        </aside>
      </div>
      ;
    </ReactPortal>
  );
}

export default MobileNavbar;
