"use client";

import { useDispatch } from "react-redux";

import Svg from "@/components/UI/Svg";
import TextLink from "@/components/UI/TextLink";
import Button from "@/components/UI/Button";

import { toggleNavbar } from "@/store/navbarSlice";

function Navbar() {
  const dispatch = useDispatch();

  return (
    <nav className="flex items-center justify-between px-[120px] py-6 max-[1300px]:px-[90px] max-[1200px]:px-[50px] max-[400px]:px-[20px]">
      <Svg
        name="elKopra"
        alt="elKopra logo"
        width={130}
        height={37}
        className="h-auto w-auto"
      />

      <ul className="flex items-center gap-10 max-[1024px]:hidden">
        <li>
          <TextLink href="/">About Us</TextLink>
        </li>
        <li>
          <TextLink href="/" className="flex gap-2">
            <span>Product</span>
            <Svg
              name="chevronDown"
              alt="chevron icon"
              width={24}
              height={24}
              className="h-auto w-auto"
            />
          </TextLink>
        </li>
        <li>
          <TextLink href="/">Project</TextLink>
        </li>
        <li>
          <TextLink href="/">Career</TextLink>
        </li>
        <li>
          <Button type="link" href="/" variant="secondary">
            Contact Us
          </Button>
        </li>
        <li>
          <div className="flex cursor-pointer items-center gap-2">
            <Svg
              name="indonesia"
              alt="indonesia flag"
              width={24}
              height={24}
              className="h-auto w-auto"
            />
            <Svg
              name="chevronDown"
              alt="chevron icon"
              width={24}
              height={24}
              className="h-auto w-auto"
            />
          </div>
        </li>
      </ul>

      <button
        onClick={() => dispatch(toggleNavbar())}
        className="flex cursor-pointer flex-col gap-1 min-[1024px]:hidden"
      >
        <div className="h-[3px] w-5 bg-white" />
        <div className="h-[3px] w-5 bg-white" />
        <div className="h-[3px] w-5 bg-white" />
      </button>
    </nav>
  );
}

export default Navbar;
