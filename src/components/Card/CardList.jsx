"use client";

import Card from "./Card";

function CardList({ list }) {
  return (
    <div className="flex flex-wrap items-stretch justify-center gap-11 px-10 max-[500px]:px-0">
      {list.map((item) => (
        <Card key={item.title} data={item} />
      ))}
    </div>
  );
}

export default CardList;
