/* eslint-disable @next/next/no-img-element */

import TextLink from "../UI/TextLink";

function Card({ data }) {
  const briefDescription =
    data?.description?.slice(0, 90) || "description is not provided";

  return (
    <div className="flex w-[344px] flex-col">
      <img
        src={data.urlToImage}
        alt={data.title}
        className="rounded-b-0 h-[207px] w-full rounded-t-2xl border border-[#184D93] object-cover"
      />
      <div className="flex flex-grow flex-col gap-4 rounded-b-2xl border border-t-0 border-[#184D93] px-4 pb-11 pt-6">
        <h3 className="h-[128px] text-2xl font-bold text-black max-[500px]:h-fit">
          {data.title}
        </h3>

        <p className="text-xs text-light-gray-2">{`${briefDescription}...`}</p>

        <TextLink href="/" className="hover:text-black">
          Learn more...
        </TextLink>
      </div>
    </div>
  );
}

export default Card;
