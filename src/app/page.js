import Hero from "@/components/Hero/Hero";
import News from "@/components/News/News";
import Statistics from "@/components/Statistics/Statistics";
import Newsletter from "@/components/Newsletter/Newsletter";

function HomePage() {
  return (
    <>
      <Hero />
      <News />
      <Statistics />
      <Newsletter />
    </>
  );
}

export default HomePage;
