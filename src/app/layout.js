import { dmSans } from "@/fonts/fonts";

import ReduxProvider from "@/components/UI/ReduxProvider";
import Footer from "@/components/Footer/Footer";
import Header from "@/components/Header/Header";

import "./globals.css";

export const metadata = {
  title: "elKopra",
  description:
    "Cooperative Management Platform that facilitates operational recording and management of cooperative members",
};

export default function RootLayout({ children }) {
  return (
    <ReduxProvider>
      <html lang="en">
        <body className={`${dmSans.className} relative`}>
          <article className="bg-indigo">
            <Header />

            <main>{children}</main>

            <Footer />
          </article>
        </body>
      </html>
    </ReduxProvider>
  );
}
