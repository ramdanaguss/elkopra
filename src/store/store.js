import { configureStore } from "@reduxjs/toolkit";
import navbarSlice from "@/store/navbarSlice";

const store = configureStore({
  reducer: {
    navbar: navbarSlice,
  },
});

export default store;
